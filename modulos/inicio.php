<?php
/*
  ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */
$pagina = new plantilla('vistas/principal.html.php');
$pastechear = new plantilla();
$cuerpo = new plantilla();
$cuerpo->time_start = microtime();
$cuerpo->carga('vistas/inicio.html.php');
$pagina->tittulo = 'Bienvenido a Pastechare';
$pagina->descripcion = 'Pastechare es un sitio para publicar y/o corregir codigo fuente, el codigo es coloreado para facilitar su lectura';
$pagina->keywords = 'pastebin,compartir codigo,pegar codigo,colorear codigo,codigo fuente';
if ($_SESSION['usuario'] === 'anonimo') {
    $paste = $pastechear->carga('vistas/paste_anonimo.html.php');
} else {
    $paste = $pastechear->carga('vistas/paste_autenticado.html.php');
}
$lenguajes = $cbd->get_results('select lenguaje,descripcion from lenguajes order by id desc');
$css = array(
    'normalize.min.css',
    'foundation.min.css',
    'foundation-icons.min.css',
    'comun.css',
    'chosen-min.css',
    'index.html.css'
);
$pagina->css = $css;
$cuerpo->lenguajes = $lenguajes;
$cuerpo->pastechear = $pastechear->inserta($paste);
$pagina->establece('cuerpo', $cuerpo->inserta());
$pagina->muestra();
unset($pagina,$cuerpo,$pastechear,$cbd);
?>
