<?php
/*
  ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */
    paranoia();
    $pagina = new plantilla("vistas/registro.html.php"); //cargo el tema padre
    $pagina->muestra(); //metodo para publicar
    unset($pagina);
?>
