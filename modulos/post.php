<?php

/*
  ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */

paranoia();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $datos_post = $_POST;
    $mensajes = new stdClass();
//$registrar = new guardar_en_bd($cbd); //<- obejto ya instanciado en el index
//$autenticar = new auth($cbd); //<- obejto ya instanciado en el index
    /* chequeo de sanidad */
    if ($autenticar->guardian_sesion() == false) {
        exit();
    }
    switch ($datos_post['accion']) {
        case 'nuevo_registro':
            $contrasena = array('contrasena' => hash('whirlpool', $datos_post['contrasena'] . $datos_post['usuario'] . "@VNX@P@st3Ch@r3")); //<- ¿como accedo a el salto de la clase auth?
            $datos_post = $registrar->reemplazar($datos_post, $contrasena);
            $extras = array('fecha_registro' => $registrar->date_time(), 'id_grupo' => '2', 'rol' => '0', 'estatus' => '1');
            $_datos = $datos_post + $extras;
            $verificar_campos = array('usuario', 'correo');
            if ($registrar->check_datos('existe', $_datos, $verificar_campos, 'usuarios') === true) {
                break;
            }
            if ($registrar->check_datos('es_nulo', $_datos, $verificar_campos, false) === true) {
                break;
            }
            $no_incluir = array('accion', 'contrasena2');
            $datos = $registrar->no_incluir($_datos, $no_incluir);
            $limpiar = new limpiador($datos, tipo_db, false);
            $registrar->guardar_datos($limpiar->campo, 'usuarios', 'json');
            break;
        case 'entrar':
            $limpiar = new limpiador($datos_post, tipo_db, true);
            $autenticar->entrar($limpiar->campo->usuario, $limpiar->campo->contrasena, 'asincrono');
            break;
        case 'pastechear':
            $url = $registrar->crc_url();
            $extras = array('id_usuario' => $autenticar->id_usuario($_SESSION['usuario']), 'id_conexion' => $registrar->id_conexion($_SESSION['token_sesion']), 'url' => $url, 'fecha_registro' => $registrar->date_time(), 'estatus' => '1');
            $_datos = $datos_post + $extras;
            $no_incluir = array('accion', 'captcha');
            $verificar_campos = array('codigo', 'titulo', 'lenguaje');
            if ($registrar->check_datos('es_nulo', $_datos, $verificar_campos, false) === true) {
                break;
            }
            if ($_SESSION['usuario'] === 'anonimo') {
                /* if(isset($datos_post['captcha']) === false){
                  $mensajes->mensaje = 'ERROR: no he podido leer el captcha';
                  echo json_encode($mensajes);
                  break;
                  }else */if (captcha::valid_code($datos_post['captcha']) === false) {
                    $mensajes->mensaje = 'ERROR: Captcha invalido, intente nuevamente';
                    echo json_encode($mensajes);
                    break;
                }
            }
            $datos = $registrar->no_incluir($_datos, $no_incluir);
            $limpiar = new limpiador($datos, tipo_db, false);
            if (ctype_digit(caracteres_permitidos)) {
                if (strlen($datos_post['codigo']) > caracteres_permitidos) {
                    $mensajes->mensaje = 'ERROR: el codigo que trata de publicar es muy largo';
                    echo json_encode($mensajes);
                    break;
                }
            }
            if ($registrar->guardar_datos($limpiar->campo, 'codigos', false) === true) {
                $mensajes->mensaje = 'ok';
                $mensajes->paste = '/paste=' . $url;
                echo json_encode($mensajes);
            }
            break;
        case 'nuevo_comentario':
            $paste = str_ireplace("=", "", stristr($_SERVER['HTTP_REFERER'], '=', false));
            $id_codigo = $cbd->get_var("select id from codigos where url='$paste'");
            $id_usuario = $autenticar->id_usuario($_SESSION['usuario']);
            if(isset($id_codigo) == false or $id_usuario === false){
                $mensajes->mensaje = 'ERROR: no puedo guardar este comentario';
                echo json_encode($mensajes);
                break;
            }
            $no_incluir = array('accion');
            $extras = array('id_usuario' => $id_usuario, 'id_codigo' => $id_codigo, 'fecha_registro' => $registrar->date_time(), 'estatus' => '1');
            $_datos = $extras + $datos_post;
            $datos = $registrar->no_incluir($_datos, $no_incluir);
            $verificar_campos = array('comentario');
            if ($registrar->check_datos('es_nulo', $datos, $verificar_campos, false) === true) {
                break;
            }
            if (strlen($datos_post['comentario']) > 255) {
                $mensajes->mensaje = 'ERROR: el comentario que trata de publicar es muy largo';
                echo json_encode($mensajes);
                break;
            }            
            $limpiar = new limpiador($datos, tipo_db, false);
            $registrar->guardar_datos($limpiar->campo, 'comentarios', 'json');
            /*if ($registrar->guardar_datos($limpiar->campo, 'comentarios', false) === true) {
                $mensajes->mensaje = 'ok';
                echo json_encode($mensajes);
            }*/
            break;
    }
    unset($mensajes, $registrar, $autenticar);  //eliminino estos objetos
}
?>
