<?php
/*
  ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */
$salir = new auth();
if($salir->salir() === true){
    $salir->redirect('/', 301);
}
unset($salir);
?>
