/*rutinas javascript's para esta pagina*/
$(document).ready(function() {
    function reset_contrasena() {
        document.getElementById("contrasena").value = ""
        document.getElementById("contrasena2").value = "";
    }
    $("#form_registro").on("valid", function(form) {
        form.stopPropagation();
        form.preventDefault();
        if (form.type === "valid") {
            usuario = document.getElementById("usuario").value;
            contrasena = document.getElementById("contrasena");
            contrasena2 = document.getElementById("contrasena2");
            if (contrasena.value !== "" && contrasena2.value !== "") {
                contrasena.value = Whirlpool(contrasena.value + usuario);
                contrasena2.value = Whirlpool(contrasena2.value + usuario);
            } else {
                reset_contrasena();
            }
            var formulario = $("#form_registro").serializeArray();
            $.ajax({
                //async: true,
                //cache: false,
                type: "POST",
                dataType: 'json',
                url: "/post",
                data: formulario,
                beforeSend: function() {
                    $(".mensaje").css("display", "block");
                    $('#mensaje').html('Procesando su registro, espere ...');
                    reset_contrasena();
                },
                success: function(respuesta) {
                    if (respuesta.mensaje === 'ok') {
                        //reset_contrasena();
                        alert('Usuario registrado con Exito!');
                        window.location = '/';
                    } else {
                        $(".mensaje").css("display", "block");
                        $("#mensaje").html(respuesta.mensaje);
                        reset_contrasena();
                    }

                }
            });
        }
    });

});



