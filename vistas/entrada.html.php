<div class="row"><br><br></div>
<div data-alert class="alert-box alert radius mensaje text-center" style="display:none;">
    <div id="mensaje"></div>
</div>
<div class="large-12 columns">
    <div class="signup-panel">
        <form data-abide="ajax" id="form_entrada">
            <div class="row collapse">
                <div class="small-2  columns">
                    <span class="prefix"><i class="fi-torso-female"></i></span>
                </div>
                <div class="small-10  columns">
                    <input type="text" name="usuario" id="usuario" placeholder="Usuario" required pattern="alpha_numeric">
                    <small class="error">El nombre de usuario es requerido.</small>
                </div>
            </div>
            <div class="row collapse">
                <div class="small-2 columns ">
                    <span class="prefix"><i class="fi-lock"></i></span>
                </div>
                <div class="small-10  columns">
                    <input type="password" name="contrasena" id="contrasena" placeholder="Contraseña" required>
                    <small class="error">Debe introducir una clave de autenticación.</small>
                </div>
            </div>
            <div class="field">
                <input type="submit" id="nueva_entrada" value="Entrar" class="button radius tiny green right"/>
                <input type="hidden" name="accion" value="entrar"/>
            </div>
        </form>
    </div>
</div>
<a class="close-reveal-modal">&#215;</a>
<script src="/js/entrada.html.js"></script>