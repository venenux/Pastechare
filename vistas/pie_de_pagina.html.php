<footer class="row">
        <div class="large-12  columns">
            <hr>
        </div>
        <div class="large-12 columns">
            <div class="large-5 columns row">
                <div class="row medium columns">
                    <a href="http://www.php.net/" target="_blank"><img  class="img_php" src="/img/php.png" alt=""></a>
                </div>
<?php if(tipo_db == 'pgsql'):?>
                <div class="row medium columns">
                    <a href="http://postgresql.org" target="_blank"><img  class="img_postgresql" src="/img/postgresql.png" alt=""></a>
                </div>
<?php endif;?>
<?php if(tipo_db == 'sqlite'):?>
                <div class="row medium columns">
                    <a href="http://sqlite.org/" target="_blank"><img  class="img_sqlite" src="/img/sqlite.png" alt=""></a>
                </div>
<?php endif;?>
                <div class="row medium columns">
                    <a href="http://validator.w3.org/check?uri=http://pastechare.linuxd.org/" target="_blank"><img  class="img_html5" src="/img/html5.png" alt=""></a>
                </div>
                <div class="row medium columns">
                    <a href="https://www.hiawatha-webserver.org/" target="_blank"><img  class="img_hiawatha" src="/img/hiawatha_88x31.png" alt=""></a>
                </div>
                <!--<div class="row large-2 columns">
                <a href="https://netbeans.org/" target="_blank"><img  class="img_netbeans" src="/img/netbeans.png" alt=""></a>
                </div> -->
            </div>
            <div class="large-5 columns">
                <ul class="inline-list ">
                    <li>
                        <a href="/archivos/">Descargar</a>
                    </li>
                    <li>
                        <a href="#">Acerca de...</a>
                    </li>
                    <li>
                        <a href="#">Filechare</a>
                    </li>
                    <li>
                        <a href="#">Picchare</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="large-12 columns">
            <div class="large-12 text-center">
                <p>Copyright © 2014 <strong>Díaz Víctor</strong> aka (Máster Vitronic) .</p>
            </div>
            <div class="large-12 text-center">
                <p><?php echo 'Generado en '. number_format(set_time($this->time_start, microtime()), 3) . ' segundos';?></p>
            </div>
        </div>
        </footer>
        <script src="/js/vendor/jquery.js"></script>
        <script src="/js/vendor/fastclick.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script src="/js/whirlpool-min.js"></script>
        <script src="/js/comun.html.js"></script>