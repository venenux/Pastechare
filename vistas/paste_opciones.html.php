<div class="row">
    <fieldset>
        <legend>Comentarios</legend>
        <div class="row large-12 columns">
            <div class="tabs-content">
                <div class="content active" id="comentarios">
                    <form id="form_comentar">
                        <div data-alert class="alert-box alert radius mensaje" style="display:none;">
                            <div id="mensaje"></div>
                        </div>
                        <div class="large-12">
                            <label>Comentario <small>obligatorio</small>
                                <textarea rows="5" cols="135" name="comentario" id="comentario" placeholder="Deje su comentario" autofocus></textarea>
                            </label>
                            <!--<small class="error">El comentario es requerido.</small>-->
                        </div>
                        <div class="field">
                            <button type="button" id="nuevo_comentario" class="tiny button green right">Comentar</button>
                            <input type="hidden" name="accion" value="nuevo_comentario"/>
                        </div>
                    </form>
                    <div id="lista_comentarios"></div>
                </div>
            </div>
        </div>
    </fieldset>
</div>
