        <form data-abide="ajax" id="form_pastechare">
            <div class="row">
                <div class="field large-12 columns">
                    <label><small></small>
                        <textarea id="codigo" class="codigo" name="codigo" rows="20" placeholder="Pegue aquí su codigo" autofocus  style="resize:none;"></textarea>
                    </label>
                    <small class="error">El codigo es requerido.</small>
                </div>
            </div>
            <div class="row">
                <fieldset>
                    <legend>Opciones de publicación</legend>
            <div class="row">
                <div class="field large-12 columns">
                    <label><small></small>
                        <input id="titulo" name="titulo" autocomplete="off" placeholder="Coloca el titulo aquí" type="text"/>
                    </label>
                    <small class="error">El titulo es requerido.</small>
                </div>
            </div>
                    <div class="large-6 large-6 columns">
                        <label>Seleccione Lenguaje</label>
                        <select id="lenguaje" name="lenguaje" class="chosen-select large-12 columns ">
<?php foreach ($this->lenguajes as $lenguaje):?>
                            <option value="<?php echo $lenguaje->lenguaje;?>"><?php echo $lenguaje->descripcion;?></option>
<?php endforeach;?>
                        </select>
                    </div>
                    <div class="large-3 columns">
                        <label>Duración</label>
                        <select id="duracion" name="duracion">
                            <option value='0'>Por siempre</option>
                            <option value='10'>10 Minutos</option>
                            <option value='60'>1 Hora</option>
                        </select>
                    </div>
                    <div class="large-3 columns">
                        <label>Exposición</label>
                        <select id="exposicion" name="exposicion">
                            <option value='1'>Publica</option>
                            <option value='0'>Privada</option>
                        </select>
                    </div>
                </fieldset>
            </div>
            <div class="row">
                <div data-alert class="alert-box alert radius mensaje_index" style="display:none;">
                    <div id="mensaje_index"></div>
                </div>
            </div>
<?php echo $this->pastechear;?>
        </form>
        <?php include_once 'vistas/pie_de_pagina.html.php'; ?>

        <script async src="/js/chosen.jquery-min.js"></script>
        <script async src="/js/index.html.js"></script>
