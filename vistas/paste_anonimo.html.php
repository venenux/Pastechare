                <div class="row">
                    <fieldset>
                        <legend>Introduce el codigo del captcha para saber que eres humano, luego pastechea</legend>
                        <div class="small has-button text-center">
                            <img id="img_captcha" src="/captcha"  alt="">
                            <a><img id="recaptcha" class="recaptcha" src="/img/recaptcha.png" title="recargar captcha"  alt=""></a>
                            <br/>
                        </div>
                        <div class="row">
                            <br/>
                            <div class="small has-button text-center">
                                <input id="captcha" class="medium-1" name="captcha" autocomplete="off" type="text" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="small has-button text-center">
                                <input type="submit" id="pastechear" value="Pastechear" class="large-3 has-button button green"/>
                                <input type="hidden" name="accion" value="pastechear"/>
                            </div>
                        </div>
                    </fieldset>
                </div>
