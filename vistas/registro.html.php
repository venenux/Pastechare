<a class="close-reveal-modal">&#215;</a>
<div class="row collapse">
    <br>
    <br>
</div>
<form  data-abide="ajax" id="form_registro">
    <div data-alert class="alert-box alert radius mensaje" style="display:none;">
        <div id="mensaje"></div>
        <!--<a href="#" class="close">&times;</a> -->
    </div>
    <div class="name-field">
        <label>Tu nombre completo <small>obligatorio</small>
            <input name="nombre" type="text" autofocus required >
        </label>
        <small class="error">Tu nombre es requerido.</small>
    </div>
    <div class="name-field">
        <label>Usuario <small>obligatorio</small>
            <input name="usuario" id="usuario"  type="text" required>
        </label>
        <small class="error">Un nombre de usuario valido es requerido.</small>
    </div>
    <div class="email-field">
        <label>Correo electronico <small>obligatorio</small>
            <input name="correo" type="email" required>
        </label>
        <small class="error">Un correo es requerido.</small>
    </div>
    <div class="password-field">
        <label for="contrasena">Contraseña <small>obligatorio</small>
            <input type="password" id="contrasena" name="contrasena"  required>
        </label>
        <small class="error">Las contraseñas deben tener al menos 4 caracteres.</small>
    </div>
    <div class="password-confirmation-field">
        <label for="contrasena2">Confirme la contraseña <small>obligatorio</small>
            <input type="password" id="contrasena2" name="contrasena2" required data-equalto="contrasena">
        </label>
        <small class="error">Las contraseñas deben se iguales.</small>
    </div>
    <div class="field">
        <input type="submit" id="nuevo_registro" value="Registrarme" class="tiny button green right">
        <input type="hidden" name="accion" value="nuevo_registro" />
    </div>
</form>
<script src="/js/registro.html.js"></script>
