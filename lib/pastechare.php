<?php

    /* lib/pastechare.php
     *
     * funciones generales de pastechare
     *   lo que no va en una clase, deberia ir aqui
     *
     *
     * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
     * Este archivo es parte de Pastechare
     * http://pastechare.linuxd.org/
     *
     * USO
     *       include 'lib/pastechare.php';
     */

    function autocarga_class($clase) {
            $locations = array("lib");
            foreach ($locations as $location) {
                    if (file_exists($file = "../".$location."/class.".$clase.".php")) {
                        require($file);
                        break;
                    }
            }
    }
    /**
     * Sets the time taken to parse the code
     *
     * @param microtime The time when parsing started
     * @param microtime The time when parsing ended
     * @since 1.0.2
     * @access private
     */
    function set_time($start_time, $end_time) {
        $start = explode(' ', $start_time);
        $end = explode(' ', $end_time);
        return $end[0] + $end[1] - $start[0] - $start[1];
    }
    
    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function validar_email($email) {
        return preg_match("/^[0-9A-Za-z]([-_.~]?[0-9A-Za-z])*@[0-9A-Za-z]([-.]?[0-9A-Za-z])*\\.[A-Za-z]{2,4}$/", $email) === 1;
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function numero_aleatorio($length = 10) {
        $source = '123456789';
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $rstr .= $source[$num - 1];
            }
        }
        return $rstr;
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function es_numero($valor) {
        return ctype_digit($valor);
    }

    // otra forma de ver si el $valor es numero
    // si es un numero valido retorna su misma cadena
    // si el valor es invalido retorna -1
    function ess_numero($valor){
        if(is_numeric($varlor)){
            return $valor;
        }else{
            return -1;
        }
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function limpia_espacios($cadena) {
        $cadena = str_replace(' ', '', $cadena);
        return $cadena;
    }

    // otra forma de limpiar espacios en blanco inicio fin es con trim()
    function limpiar_cadena($cadena){
        return trim($cadena);
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function telefono_valido($telefono) {
        $phonenr = str_replace(" ", "", $telefono);
        return preg_match("/^([1]-)?[0-9]{4}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/i", $phonenr) === 1;
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function fecha_valida($fecha) {
        return preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $fecha) === 1;
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function html2txt($document) {
        $search = array('@<script[^>]*?>.*?</script>@si', // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@', // Strip multi-line comments including CDATA
            '@<div[^>]*?>.*?</div>@si'
        );
        $text = preg_replace($search, '', $document);
        return $text;
    }

    /* Falta documentar
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function hms2sec($hms) {
        list($h, $m, $s) = explode(":", $hms);
        $seconds = 0;
        $seconds += (intval($h) * 3600);
        $seconds += (intval($m) * 60);
        $seconds += (intval($s));
        return $seconds;
    }

    /** esto deberia implementarlo en una clase
     *
     * Modo paranoia, si se llama directo a este script
     * lo mando a la entrada
     */
    function paranoia() {
        if (empty($_SERVER['HTTP_REFERER'])) {
            //header('Location: /', true, 301);
            require_once "modulos/error.php";
        }
    }

?>
