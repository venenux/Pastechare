<?php


/* lib/class.auth.php
 *
 * Clase de autenticacion y validacion de usuarios
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * Copyright (C) 2014 by Misa3l  <blacksecured@gmail.com>
 * Este archivo es parte de Pastechare
 * http://pastechare.linuxd.org/
 *
 * USO
 *      Autenticar
 *       $autenticar = new auth($cbd);
 *       $autenticar->entrar('anonimo', 'e5b9e3f36a6ae6608c6ec1777cdef6a3')
 *
 *      Por hacer
 *          implemetar un metodo sincrono-asincrono de coneccion
 *
 */

class auth {

    private $dbdc; /* propiedad data base desde clase :-D */
    public $salto = "@VNX@P@st3Ch@r3"; /* propiedad salto, un poco de sal al passwod */
    private $_usuario; /* propiedad usuario */
    private $_contrasena; /* propiedad contraseña */

    /* Contructor, inicializa la clase
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __construct($dbdc = null) {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($dbdc)) {
            $this->dbdc = $dbdc;
        }
    }

    /* Destructor, destruye automaticamente la clase.
     * Aunque es necesario destruir el objeto luego de
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __destruct() {
        if (!isset($_SESSION['vida_sesion'])) {
            $this->llave('salir', '');
        }
    }

    /* Inicia/Cierra sesión, se puede mejorar, eso creo
     *
     * ENTRADA: - dos cadenas, el modo (entrar,salir) y el nombre del usuario
     * SALIDA:  -
     * ERROR:   -
     */

    private function llave($modo, $usuario = '') {
        $this->usuario = $usuario;
        $this->modo = $modo;
        switch ($modo) {
            case 'salir':
                unset($_SESSION['logueado'], $_SESSION['usuario'], $_SESSION['vida_sesion'], $_SESSION['token_sesion'], $_SESSION["captcha_code"]);
                $_SESSION = array();
                return true;
                break;
            case 'entrar':
                $_SESSION['logueado'] = true;
                $_SESSION['usuario'] = $this->usuario;
                $this->renovar_sesion();
                $_SESSION['token_sesion'] = md5(uniqid(rand(), 1));
                return true;
                break;
        }
    }

    /* Redirecciona al usuario
     *
     * ENTRADA: - url destino / codigo de redireccion
     * SALIDA:  -
     * ERROR:   -
     */

    public function redirect($url, $codigo) {
        $this->url = $url;
        $this->codigo = $codigo;
        if (isset($this->url) and isset($this->codigo)) {
            return header("Location: $this->url", true, $this->codigo);
        } else {
            return null;
        }
    }

    /* Permite/Deniega la entrada de un usuario al sistema
     *
     * ENTRADA: - dos cadenas, el usuario y la contraseña
     * SALIDA:  -
     * ERROR:   - 1 = usuario bloqueado por la administracion
     *            2 = Usuario / Clave Invalido
     *            3 = fallo de todas las condiciones (improbable)
     */

    public function entrar($_usuario, $_contrasena, $modo = 'sincrono') {
        $this->_usuario = $_usuario;
        $this->_contrasena = $_contrasena;
        if ($this->_usuario !== 'anonimo') {
            $this->_contrasena = hash('whirlpool', $this->_contrasena . $this->_usuario . $this->salto);
        }
        $datos = $this->dbdc->get_row("select id,usuario,estatus from usuarios where
                                      usuario='$this->_usuario' and contrasena='$this->_contrasena' ");
        switch ($modo) {
            case 'sincrono':
                if (isset($datos->id) and $datos->estatus == 0) {
                    return 1;
                } elseif (isset($datos->id) and $datos->id > 0) {
                    $this->llave('entrar', $datos->usuario);
                    //$this->redirect('/', 301);  //la redireccion aqui me da problemas con los bot de internet
                } elseif (empty($datos->id)) {
                    return 2;
                } else {
                    return 3;
                }
                break;
            case 'asincrono':
                $mensajes = new stdClass();
                if (isset($datos->id) and $datos->estatus == 0) {
                    $mensajes->mensaje = 'Usuario bloqueado por la administración';
                    echo json_encode($mensajes);
                } elseif (isset($datos->id) and $datos->id > 0) {
                    $this->llave('entrar', $datos->usuario);
                    $mensajes->mensaje = 'ok';
                    echo json_encode($mensajes);
                } elseif (empty($datos->id)) {
                    $mensajes->mensaje = 'Usuario o contraseña invalido';
                    echo json_encode($mensajes);
                } else {
                    $mensajes->mensaje = 'Error, Will Robinson!';
                    echo json_encode($mensajes);
                }
                unset($mensajes);
                break;
        }
    }

    /* Actualiza el tiempo de vida de sesion del usuario
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    private function renovar_sesion() {
        $_SESSION['vida_sesion'] = time();
    }

    /* Verifica que la sesion no alla exedido
     * el tiempo configurado, si lo hizo entonces
     * cierra la sesion, si no entonces la mantiene abierta
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function vida_sesion() {
        if (ctype_digit(sesion_expira)) {
            if (isset($_SESSION['vida_sesion'])) {
                $vida_session = time() - $_SESSION['vida_sesion'];
                if ($vida_session > sesion_expira) {
                    $this->salir();
                    return false;
                } else {
                    $this->renovar_sesion();
                    return true;
                }
            }
        }
    }

    /* Verifica si un usuario esta autenticado,
     * si esta autenticado entonces invova a vida_sesion()
     *
     *
     * ENTRADA: -
     * SALIDA:  - boolean
     * ERROR:   -
     */

    public function guardian_sesion() {
        if ($this->vida_sesion() == false) {
            return false;
        } else {
            return true;
        }
    }

    /* autentica al visitante asignandole el usuario anonimo
     *
     * ENTRADA: -
     * SALIDA:  - true
     * ERROR:   - false
     */

    public function sesion_anonima() {
        if (!isset($_SESSION['usuario'])) {
            $this->entrar('anonimo', 'null');
            return true;
        } else {
            return false;
        }
    }

    /* retorna el id de un usuario
     *
     * ENTRADA: - $_SESSION['usuario']
     * SALIDA:  - id_usuario
     * ERROR:   - false
     */

    public function id_usuario($usuario) {
        $id_usuario = $this->dbdc->get_var("select id from usuarios where usuario='$usuario'");
        if ($id_usuario) {
            return $id_usuario;
        } else {
            return false;
        }
    }

    /* Cerar sesion y salir del sistema limpiamente
     *
     * ENTRADA: -
     * SALIDA:  - true
     * ERROR:   - false
     */

    public function salir() {
        if ($this->llave('salir', '')) {
            return true;
        } else {
            return false;
        }
    }

}

?>
