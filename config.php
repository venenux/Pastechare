<?php

/* lib/config.php
 *
 * archivo de configuracion
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * Este archivo es parte de Pastechare
 * http://pastechare.linuxd.org/
 */

include_once 'lib/ezsql/ez_sql_core.php';
include_once 'lib/class.config.php';
new config('pastechare.ini');
/* seteo la fecha en español esto depende del enviroment del sistema, chequear locale_gen */
    date_default_timezone_set("America/Caracas");
    setlocale(LC_TIME, "es_ES.UTF-8");
    error_reporting(-1);
    if (tipo_db === 'pgsql') {
        /* Me conecto a la base de dato PostgreSQL */
        include_once 'lib/ezsql/ez_sql_postgresql.php';
        $cbd = new ezSQL_postgresql(usuario_pgsql, passwd_pgsql, bd_pgsql, host_pgsql);
    } elseif (tipo_db === 'sqlite') {
        include_once 'lib/ezsql/ez_sql_pdo.php';
        /* Me conecto a la base de dato SQLite 3 */
        $cbd = new ezSQL_pdo('sqlite:' . sqlite_db, ' ', ' ');
        $cbd->query('PRAGMA foreign_keys = ON;');
    }
        $cbd->cache_dir = 'cache_sql';
        $cbd->use_disk_cache = true;
        $cbd->cache_queries = true;
        $cbd->cache_timeout = 24;
?>
