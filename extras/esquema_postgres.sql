/*
   ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */

drop table grupos,usuarios,conexiones,codigos,lenguajes,comentarios;

create table if not exists grupos( --Tabla grupos PostgreSQL
    id serial not null primary key, -- id de esta columna
    grupo varchar(64) not null, --nombre de este grupo
    descripcion varchar(255) not null, --descripcion de este grupo
    estatus smallint not null default 1 -- estatus de este grupo 1 = activo 0 = inactivo
);create unique index grupos_id on grupos (id);
create unique index grupos_grupo on grupos (grupo);
alter table public.grupos OWNER to pastechare_user;
alter sequence grupos_id_seq RESTART with 3;
insert into grupos (id,grupo,descripcion,estatus)values(1,'admin','grupo de administradores',1);
insert into grupos (id,grupo,descripcion,estatus)values(2,'predeterminado','grupo predeterminado',1);

create table if not exists usuarios( --Tabla usuarios PostgreSQL
    id serial not null primary key, -- id de esta columna
    nombre varchar(50) not null, --nombre completo
    usuario varchar(24) not null, --aka,alias,sobrenombre de este usuario
    correo varchar(25) not null, -- agregado el campo para el correo electronico
    contrasena varchar(128) not null, -- contraseña de este usuario hash whirlpool
    fecha_registro  timestamp default current_timestamp  not null , --la fecha de este registro
    id_grupo int not null, -- id del grupo , relacionada a la tabla grupos
    rol smallint not null default 0, -- rol de este usuario por defecto 0 = usuario 1 = admin ?= mas roles aun no estan definidos
    estatus smallint not null default 1, -- estatus de este grupo 1 = activo 0 = inactivo
    foreign key(id_grupo) references grupos(id)
);create unique index usuarios_id on usuarios (id);
create unique index usuarios_usuario on usuarios (usuario);
alter table public.usuarios OWNER to pastechare_user;
alter sequence usuarios_id_seq RESTART with 3;
insert into usuarios (id,nombre,usuario,correo,contrasena,fecha_registro,id_grupo,rol,estatus)values(1,'Usuario Anonimo','anonimo','null@null','null','2014-05-16 16:17:57.865351',2,0,1);
insert into usuarios (id,nombre,usuario,correo,contrasena,fecha_registro,id_grupo,rol,estatus)values(2,'Máster Vitronic','vitronic','vitronic2@gmail.com','e61b442af8f461f2476c86d81b54f36a398af75beb2278bafd7ea395468934cc41d7915c5911f6d9260093bf655f15181dd10a8ed064e6b740a2a873cacba389','2014-05-16 16:17:57.865351',1,1,1);

create table if not exists conexiones( --Tabla conexiones PostgreSQL
    id serial not null primary key, -- id de esta columna
    id_usuario int not null, -- id del usuario , relacionada a la tabla usuarios
    token_sesion varchar(32), --token unico que identificara esta conexion
    ip_sesion varchar(15) not null default '000.000.000.000', -- la ip desde la cual se conecta
    fecha_registro  timestamp default current_timestamp  not null , --la fecha de este registro
    estatus smallint not null default 0, -- estatus de este grupo 1 = activo 0 = inactivo
    foreign key(id_usuario) references usuarios(id)
);create unique index conexiones_id on conexiones (id);
alter table public.conexiones OWNER to pastechare_user;

create table if not exists codigos( --Tabla codigos PostgreSQL
    id serial not null primary key, -- id de esta columna
    id_usuario int not null, --id del usuario , relacionada a la tabla usuarios
    id_conexion int not null, --id de la conexion, relacionada a la tabla conexiones
    url varchar(32), --Cyclic redundancy check codigo de 8 digitos que sera usado para la url
    codigo text, --aqui se guardara todo el codigo
    titulo varchar(32), --titulo de esta publicacion
    lenguaje varchar(32), --lenguaje de programacion de este codigo
    fecha_registro  timestamp default current_timestamp  not null , --la fecha de este registro
    exposicion int not null default 1, --modo de este registro 1 = publico 0 = privado
    duracion int not null default 0, -- numero de minutos que durara este pastechare , 0 es por siempre
    estatus smallint not null default 0, -- estatus de este grupo 1 = activo 0 = inactivo
    foreign key(id_usuario) references usuarios(id),
    foreign key(id_conexion) references conexiones(id)
);create unique index codigos_id on codigos (id);
create unique index codigos_url on codigos (url);
alter table public.codigos OWNER to pastechare_user;

create table if not exists lenguajes( --Tabla lenguajes PostgreSQL
    id serial not null primary key, -- id de esta columna
    lenguaje  varchar(32) not null, -- nombre del lenguaje de programacion
    descripcion varchar(128) not null -- descripcion del lenguaje de programacion
);create unique index lenguajes_id on lenguajes (id);
create unique index lenguajes_lenguaje on lenguajes (lenguaje);
alter table public.lenguajes OWNER to pastechare_user;
alter sequence lenguajes_id_seq RESTART with 235;


create table if not exists comentarios( --Tabla comentarios PostgreSQL
    id serial not null primary key, -- id de esta columna
    id_usuario int not null, -- id del usuario , relacionada a la tabla usuarios
    id_codigo int not null, -- id del codigo,relacionada a la tabla conexiones
    comentario varchar(255) not null, --el comentario
    fecha_registro  timestamp default current_timestamp  not null , --la fecha de este registro
    estatus smallint not null default 0, -- estatus de este grupo 1 = activo 0 = inactivo
    foreign key(id_usuario) references usuarios(id),
    foreign key(id_codigo) references codigos(id)
);create unique index comentarios_id on comentarios (id);
alter table public.comentarios OWNER to pastechare_user;

create table if not exists replicas( --Tabla replicas PostgreSQL
    id serial not null primary key, -- id de esta columna
    id_usuario int not null, -- id del usuario , relacionada a la tabla usuarios
    id_comentario int not null, -- id del comentario,relacionada a la tabla comentarios
    replica varchar(255) not null, --el comentario
    fecha_registro  timestamp default current_timestamp  not null , --la fecha de este registro
    estatus smallint not null default 0, -- estatus de este grupo 1 = activo 0 = inactivo
    foreign key(id_usuario) references usuarios(id),
    foreign key(id_comentario) references comentarios (id)
);create unique index replicas_id on replicas (id);
alter table public.replicas OWNER to pastechare_user;


